Feature: Test if membership should have been renewed already

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId | website | status    | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 123      | ES      | ACTIVATED | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   |
      | 124      | ES      | ACTIVATED | ENABLED     | 123             | 2017-07-06     | 2025-07-06     | 54.99   |
      | 125      | ES      | EXPIRED   | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   |

  Scenario Outline: Should return true when membership is active and past expiration date
    When check if <membershipId> needs to be renewed
    Then the member is to be renewed
    Examples:
      | membershipId |
      | 123          |

  Scenario Outline: Should return false when membership is active but not past expiration date
    When check if <membershipId> needs to be renewed
    Then the member is not to be renewed
    Examples:
      | membershipId |
      | 124          |
      | 125          |
