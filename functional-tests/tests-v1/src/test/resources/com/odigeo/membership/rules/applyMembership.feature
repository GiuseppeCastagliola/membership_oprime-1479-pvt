Feature: Functional test for ApplyMembership endpoint service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 1234            | 456    | ELLEN     | RIPLEY    |
    And the next membership stored in db:
      | memberId | website | status    | autoRenewal | memberAccountId | activationDate | expirationDate | balance | productStatus | sourceType     |
      | 1234     | ES      | ACTIVATED | ENABLED     | 1234            | 2019-11-02     | 2020-11-02     | 54.99   | null          | FUNNEL_BOOKING |

  Scenario Outline: Check that the membership is applicable or not for a passenger into the passengers list
    Given the next checkMemberOnPassengerListRequest:
      | userId | site |
      | 456    | ES   |
    And the next checkMemberOnPassengerListRequest includes the following passengers:
      | name    | lastNames    |
      | JOHN    | WICK         |
      | <name>  | <lastNames>  |
      | JOHN    | MCCLANE      |
    When requested to apply membership with checkMemberOnPassengerListRequest:
    Then the membership <applicable> applicable for this passengers list:
    Examples:
    | name    | lastNames   | applicable  |
    | ELLEN   | RIPLEY      | is          |
    | JOHN    | RAMBO       | is not      |