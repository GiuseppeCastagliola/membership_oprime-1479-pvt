package com.odigeo.membership.functionals.membership.product;

import com.odigeo.membership.request.product.creation.CreateBasicFreeMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.request.product.creation.UserCreationInfo;

import java.math.BigDecimal;

import static java.util.Objects.nonNull;

public class CreateMembershipRequestBuilder {

    private String website;
    private int monthsToRenewal;
    private String sourceType;
    private String membershipType;

    //pendingToCollect fields
    private long memberAccountId;
    private String expirationDate;
    private BigDecimal subscriptionPrice;
    private String currencyCode;
    private String recurringId;

    //createNewMembership fields
    private String userId;
    private String name;
    private String lastNames;
    private String email;
    private String locale;
    private Integer trafficInterfaceId;
    //basicFree fields
    private String channel;

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getMonthsToRenewal() {
        return monthsToRenewal;
    }

    public void setMonthsToRenewal(int monthsToRenewal) {
        this.monthsToRenewal = monthsToRenewal;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Integer getTrafficInterfaceId() {
        return trafficInterfaceId;
    }

    public void setTrafficInterfaceId(Integer trafficInterfaceId) {
        this.trafficInterfaceId = trafficInterfaceId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public CreateMembershipRequest buildPendingToCollect() {
        return new CreatePendingToCollectRequest.Builder()
                .withMemberAccountId(this.memberAccountId)
                .withExpirationDate(this.expirationDate)
                .withSubscriptionPrice(this.subscriptionPrice)
                .withCurrencyCode(this.currencyCode)
                .withRecurringId(this.recurringId)
                .withWebsite(this.website)
                .withMonthsToRenewal(this.monthsToRenewal)
                .withSourceType(this.sourceType)
                .withMembershipType(this.membershipType)
                .build();
    }

    public CreateMembershipRequest buildCreateNewMembership() {
        UserCreationInfo userCreationInfo = new UserCreationInfo.Builder()
                .withEmail(this.email)
                .withLocale(this.locale)
                .withTrafficInterfaceId(this.trafficInterfaceId)
                .build();
        return new CreateNewMembershipRequest.Builder()
                .withWebsite(this.website)
                .withMonthsToRenewal(this.monthsToRenewal)
                .withSourceType(this.sourceType)
                .withMembershipType(this.membershipType)
                .withUserId(this.userId)
                .withName(this.name)
                .withLastNames(this.lastNames)
                .withUserCreationInfo(userCreationInfo)
                .build();
    }

    public CreateMembershipRequest buildCreateBasicFreeMembership() {
        UserCreationInfo userCreationInfo = null;
        if (nonNull(this.email)) {
            userCreationInfo = new UserCreationInfo.Builder()
                    .withEmail(this.email)
                    .withLocale(this.locale)
                    .withTrafficInterfaceId(this.trafficInterfaceId)
                    .build();
        }
        return new CreateBasicFreeMembershipRequest.Builder()
                .withWebsite(this.website)
                .withMonthsToRenewal(this.monthsToRenewal)
                .withSourceType(this.sourceType)
                .withMembershipType(this.membershipType)
                .withUserId(this.userId)
                .withName(this.name)
                .withLastNames(this.lastNames)
                .withUserCreationInfo(userCreationInfo)
                .withCurrencyCode(this.currencyCode)
                .withChannel(this.channel)
                .build();
    }

}
