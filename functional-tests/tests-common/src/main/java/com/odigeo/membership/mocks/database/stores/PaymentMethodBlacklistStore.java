package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.BlacklistPaymentMethodBuilder;
import com.odigeo.membership.util.DateConverter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class PaymentMethodBlacklistStore {

    private static final String SAVE_BLACKLIST_PAYMENT_METHODS = "INSERT INTO MEMBERSHIP_OWN.GE_PAYMENT_METHOD_BLACKLIST (ID, ERROR_MESSAGE, ERROR_TYPE, TIMESTAMP, MEMBERSHIP_ID) VALUES (?, ?, ?, ?, ?)";

    public void saveBlacklistPaymentMethods(Connection conn, List<BlacklistPaymentMethodBuilder> paymentMethods) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_BLACKLIST_PAYMENT_METHODS)) {
            for (BlacklistPaymentMethodBuilder paymentMethod : paymentMethods) {
                pstmt.setString(1, paymentMethod.getId());
                pstmt.setString(2, paymentMethod.getErrorMessage());
                pstmt.setString(3, paymentMethod.getErrorType());
                pstmt.setDate(4, new Date(DateConverter.toIsoDate(paymentMethod.getTimestamp()).map(java.util.Date::getTime).get()));
                pstmt.setString(5, paymentMethod.getMembershipId());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

}
