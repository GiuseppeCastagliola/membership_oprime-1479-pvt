package com.odigeo.membership.functionals.membership.booking;

import com.odigeo.membership.util.DateConverter;

import java.math.BigDecimal;
import java.util.Date;

public class MembershipBookingBuilder {
    private long bookingId;
    private long memberId;
    private Date bookingDate;
    private String bookingDateIso;
    private BigDecimal avoidFeeAmount;
    private BigDecimal costFeeAmount;
    private BigDecimal perksFeeAmount;


    public long getBookingId() {
        return bookingId;
    }

    public MembershipBookingBuilder setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public long getMemberId() {
        return memberId;
    }

    public MembershipBookingBuilder setMemberId(long memberId) {
        this.memberId = memberId;
        return this;
    }

    public Date getBookingDate() {
        return bookingDate != null ? (Date) bookingDate.clone() : getBookingDateIso();
    }

    public MembershipBookingBuilder setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate != null ? (Date) bookingDate.clone() : getBookingDateIso();
        return this;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public MembershipBookingBuilder setAvoidFeeAmount(BigDecimal avoidFeeAmount) {
        this.avoidFeeAmount = avoidFeeAmount;
        return this;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public MembershipBookingBuilder setCostFeeAmount(BigDecimal costFeeAmount) {
        this.costFeeAmount = costFeeAmount;
        return this;
    }

    public BigDecimal getPerksFeeAmount() {
        return perksFeeAmount;
    }

    public MembershipBookingBuilder setPerksFeeAmount(BigDecimal perksFeeAmount) {
        this.perksFeeAmount = perksFeeAmount;
        return this;
    }

    private Date getBookingDateIso() {
        return DateConverter.toIsoDate(bookingDateIso)
                .orElse(null);
    }

    public void setBookingDateIso(final String bookingDateIso) {
        this.bookingDateIso = bookingDateIso;
    }

    @Override
    public String toString() {
        return "MembershipBookingBuilder{"
                + "bookingId=" + bookingId
                + ", memberId=" + memberId
                + ", bookingDate=" + bookingDate
                + ", bookingDateIso=" + bookingDateIso
                + ", avoidFeeAmount=" + avoidFeeAmount
                + ", costFeeAmount=" + costFeeAmount
                + ", perksFeeAmount=" + perksFeeAmount
                + '}';
    }
}
