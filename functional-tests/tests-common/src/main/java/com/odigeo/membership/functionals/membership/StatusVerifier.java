package com.odigeo.membership.functionals.membership;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class StatusVerifier {
    private final String productStatus;
    private final String membershipStatus;
    private final BigDecimal balance;
    private final String activationDate;

    public StatusVerifier(String productStatus, String membershipStatus, BigDecimal balance, String activationDate) {
        this.productStatus = productStatus;
        this.membershipStatus = membershipStatus;
        this.balance = balance;
        this.activationDate = activationDate;
    }

    public void verifyStatus(String membershipStatus, String productStatus, BigDecimal balance, String activationDate) {
        assertEquals(productStatus, this.productStatus);
        assertEquals(membershipStatus, this.membershipStatus);
        if (nonNull(this.balance)) {
            assertEquals(balance.setScale(2, RoundingMode.UP), this.balance.setScale(2, RoundingMode.UP));
        }
        if (nonNull(this.activationDate)) {
            assertTrue(activationDate.startsWith("today".equals(this.activationDate) ? LocalDate.now().format(DateTimeFormatter.ISO_DATE) : this.activationDate));
        }
    }
}
