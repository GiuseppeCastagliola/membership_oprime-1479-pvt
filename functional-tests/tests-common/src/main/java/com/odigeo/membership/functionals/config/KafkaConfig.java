package com.odigeo.membership.functionals.config;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class KafkaConfig {
    private static final String URL_FORMAT = "{host}:{port}";
    private static final String URL_HOST = "{host}";
    private static final String URL_PORT = "{port}";
    private static final Integer PORT = 9092;
    private static final Integer ZOO_KEEPER_PORT = 2181;
    private static final String SUBSCRIPTION_MSG_TOPIC_NAME = "MEMBERSHIP_ACTIVATIONS_v4";
    private static final String SUBSCRIPTION_MSG_GROUP_ID = "KafkaPollerFunctionalTest";
    private static final String ACTIVATION_RETRY_MSG_TOPIC_NAME = "MEMBERSHIP_RETRY_ACTIVATIONS";
    private static final String ACTIVATION_RETRY_MSG_GROUP_ID = "MembershipRetryRobot";
    private static final String MEMBERSHIP_UPDATE_MSG_TOPIC_NAME = "MEMBERSHIP_UPDATES_V1";
    private static final String MEMBERSHIP_UPDATE_MSG_GROUP_ID = "UpdatesKafkaPollerFunctionalTest";

    private String localIp;

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public String getHost() {
        return localIp;
    }

    public Integer getPort() {
        return PORT;
    }

    public Integer getZooKeeperPort() {
        return ZOO_KEEPER_PORT;
    }

    public String getSubscriptionMessagesTopicName() {
        return SUBSCRIPTION_MSG_TOPIC_NAME;
    }

    public String getSubscriptionMessagesGroupId() {
        return SUBSCRIPTION_MSG_GROUP_ID;
    }

    public String getActivationRetryMessagesTopicName() {
        return ACTIVATION_RETRY_MSG_TOPIC_NAME;
    }

    public String getActivationRetryMessagesGroupId() {
        return ACTIVATION_RETRY_MSG_GROUP_ID;
    }

    public String getCompleteHost() {
        return URL_FORMAT.replace(URL_HOST, getHost()).replace(URL_PORT, PORT.toString());
    }

    public String getCompleteZooKeeperHost() {
        return URL_FORMAT.replace(URL_HOST, getHost()).replace(URL_PORT, ZOO_KEEPER_PORT.toString());
    }

    public String getMembershipUpdateMessagesTopicName() {
        return MEMBERSHIP_UPDATE_MSG_TOPIC_NAME;
    }

    public String getMembershipUpdateMessagesGroupId() {
        return MEMBERSHIP_UPDATE_MSG_GROUP_ID;
    }
}
