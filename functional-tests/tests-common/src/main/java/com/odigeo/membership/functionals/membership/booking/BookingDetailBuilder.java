package com.odigeo.membership.functionals.membership.booking;

import java.math.BigDecimal;

public class BookingDetailBuilder {

    private Long bookingId;
    private boolean hasPerks;
    private Long memberId;
    private String bookingStatus;
    private BigDecimal avoidFeeAmount;
    private BigDecimal costFeeAmount;
    private BigDecimal perksFeeAmount;
    private String creationDate;
    private String productType;
    private String website;
    private String email;
    private Boolean testBooking;

    public Long getBookingId() {
        return bookingId;
    }

    public BookingDetailBuilder setBookingId(final Long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public boolean hasPerks() {
        return hasPerks;
    }

    public BookingDetailBuilder setHasPerks(final boolean hasPerks) {
        this.hasPerks = hasPerks;
        return this;
    }

    public Long getMemberId() {
        return memberId;
    }

    public BookingDetailBuilder setMemberId(final Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public BookingDetailBuilder setBookingStatus(final String bookingStatus) {
        this.bookingStatus = bookingStatus;
        return this;
    }

    public BigDecimal getAvoidFeeAmount() {
        return avoidFeeAmount;
    }

    public BookingDetailBuilder setAvoidFeeAmount(final BigDecimal avoidFeeAmount) {
        this.avoidFeeAmount = avoidFeeAmount;
        return this;
    }

    public BigDecimal getCostFeeAmount() {
        return costFeeAmount;
    }

    public BookingDetailBuilder setCostFeeAmount(final BigDecimal costFeeAmount) {
        this.costFeeAmount = costFeeAmount;
        return this;
    }

    public BigDecimal getPerksFeeAmount() {
        return perksFeeAmount;
    }

    public BookingDetailBuilder setPerksFeeAmount(final BigDecimal perksFeeAmount) {
        this.perksFeeAmount = perksFeeAmount;
        return this;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public BookingDetailBuilder setCreationDate(final String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public String getProductType() {
        return productType;
    }

    public BookingDetailBuilder setProductType(String productType) {
        this.productType = productType;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public BookingDetailBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public BookingDetailBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public Boolean getTestBooking() {
        return testBooking;
    }

    public void setTestBooking(Boolean testBooking) {
        this.testBooking = testBooking;
    }
}
