package com.odigeo.membership.parameters.request;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.testng.Assert.assertEquals;

public class MembershipCreationBuilderTest {

    private static final int MONTHS_DURATION_DEFAULT = 12;

    private static final Long USER_ID = 1L;
    private static final String NAME = "Yukihiro";
    private static final String LAST_NAME = "Matsumoto";
    private static final String WEBSITE = "IT";
    private static final MemberStatus MEMBER_STATUS = MemberStatus.ACTIVATED;
    private static final LocalDate ACTIVATION_DATE = LocalDate.now();
    private static final LocalDate EXPIRATION_DATE = LocalDate.now();
    private static final StatusAction STATUS_ACTION = StatusAction.CREATION;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final String MONTHS_DURATION = "6";
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;

    @Test
    public void testBuilderWithCorrectMonthsDuration() throws Exception {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        validateCommonFields(membershipCreationBuilder);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), Integer.parseInt(MONTHS_DURATION));
    }

    @Test
    public void testBuilderWithoutMonthsDuration() throws Exception {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration(null);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    @Test
    public void testBuilderWithEmptyDuration() throws Exception {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration(StringUtils.EMPTY);
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    @Test
    public void testBuilderWithZeroDuration() throws Exception {
        MembershipCreationBuilder membershipCreationBuilder = buildMembershipCreationBuilderByDefault();
        membershipCreationBuilder.withMonthsDuration("0");
        assertEquals(membershipCreationBuilder.getMonthsDuration(), MONTHS_DURATION_DEFAULT);
    }

    private void validateCommonFields(MembershipCreationBuilder membershipCreationBuilder) {
        assertEquals(membershipCreationBuilder.getActivationDate(), ACTIVATION_DATE);
        assertEquals(membershipCreationBuilder.getExpirationDate(), EXPIRATION_DATE);
        assertEquals(membershipCreationBuilder.getLastNames(), LAST_NAME);
        assertEquals(membershipCreationBuilder.getMembershipType(), MEMBERSHIP_TYPE);
        assertEquals(membershipCreationBuilder.getMemberStatus(), MEMBER_STATUS);
        assertEquals(membershipCreationBuilder.getName(), NAME);
        assertEquals(membershipCreationBuilder.getSourceType(), SOURCE_TYPE);
        assertEquals(membershipCreationBuilder.getStatusAction(), STATUS_ACTION);
        assertEquals(membershipCreationBuilder.getSubscriptionPrice(), SUBSCRIPTION_PRICE);
        assertEquals(membershipCreationBuilder.getUserId(), USER_ID);
    }

    private MembershipCreationBuilder buildMembershipCreationBuilderByDefault() {
        MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder();
        membershipCreationBuilder.withActivationDate(ACTIVATION_DATE);
        membershipCreationBuilder.withExpirationDate(EXPIRATION_DATE);
        membershipCreationBuilder.withLastNames(LAST_NAME);
        membershipCreationBuilder.withName(NAME);
        membershipCreationBuilder.withMembershipType(MEMBERSHIP_TYPE);
        membershipCreationBuilder.withWebsite(WEBSITE);
        membershipCreationBuilder.withMemberStatus(MEMBER_STATUS);
        membershipCreationBuilder.withStatusAction(STATUS_ACTION);
        membershipCreationBuilder.withUserId(USER_ID);
        membershipCreationBuilder.withMonthsDuration(MONTHS_DURATION);
        membershipCreationBuilder.withSourceType(SOURCE_TYPE);
        membershipCreationBuilder.withSubscriptionPrice(SUBSCRIPTION_PRICE);
        return membershipCreationBuilder;
    }

}
