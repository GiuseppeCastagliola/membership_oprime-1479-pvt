package com.odigeo.membership;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MembershipFeeTest extends BeanTest<MembershipFee> {

    private static final String MEMBERSHIP_ID = "123";
    private static final BigDecimal AMOUNT = BigDecimal.TEN;
    private static final String CURRENCY = "EUR";
    private static final String FEE_TYPE = "RENEWAL";

    @Override
    protected MembershipFee getBean() {
        return new MembershipFee(MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);
    }

    @Test
    public void testEquals() throws Exception {
        MembershipFee mbfee1 = new MembershipFee(MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);
        MembershipFee mbfee2 = new MembershipFee(MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);
        assertEquals(mbfee1, mbfee2);
    }

    @Test
    public void testHashCode() throws Exception {
        MembershipFee mbfee1 = new MembershipFee(MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE);
        assertNotNull(mbfee1.hashCode());
    }

}