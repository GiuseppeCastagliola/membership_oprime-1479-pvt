package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.Membership;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;

public class ValidationHelperTest {

    private static final String EXPECTED_ILLEGAL_ARGUMENT_MESSAGE = "At least one validation to be run is needed";

    @Test(expectedExceptions = IllegalArgumentException.class,
            expectedExceptionsMessageRegExp = EXPECTED_ILLEGAL_ARGUMENT_MESSAGE)
    public void testRunValidationsThrowsIAEIfEmptyListGiven() {
        assertFalse(ValidationHelper.runValidations(Collections.EMPTY_LIST));
    }

    @Test(expectedExceptions = IllegalArgumentException.class,
            expectedExceptionsMessageRegExp = EXPECTED_ILLEGAL_ARGUMENT_MESSAGE)
    public void testRunValidationsThrowsIAEIfNullGiven() {
        assertFalse(ValidationHelper.runValidations(null));
    }

    @Test
    public void testRunValidationsReturnsFalseWithInconsistentValidations() {
        assertFalse(ValidationHelper.runValidations(assembleConsistentValidations(false)));
    }

    private List<Validation> assembleConsistentValidations(final boolean isConsistentList) {
        return isConsistentList ? Arrays.asList(new MemberHasMembershipInactiveValidation(mock(Membership.class)))
                : Arrays.asList(new BookingHasContractStatusValidation(mock(BookingDetail.class)));
    }

}
