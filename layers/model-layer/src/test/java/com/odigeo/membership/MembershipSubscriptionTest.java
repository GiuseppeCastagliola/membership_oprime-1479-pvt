package com.odigeo.membership;

import bean.test.BeanTest;

public class MembershipSubscriptionTest extends BeanTest<MembershipSubscription> {

    private static Long productId = 1L;
    private static String website = "ES";

    @Override
    protected MembershipSubscription getBean() {
        return new MembershipSubscription(productId, website);
    }
}