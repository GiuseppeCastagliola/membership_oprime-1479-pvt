package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class LogoutRequestTest extends BeanTest<LogoutRequest> {
    @Test
    public void logoutRequestEqualsVerifierTest() {
        EqualsVerifier.forClass(LogoutRequest.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected LogoutRequest getBean() {
        return assembleLogoutRequest();
    }

    private LogoutRequest assembleLogoutRequest() {
        LogoutRequest logoutRequest = new LogoutRequest();
        logoutRequest.setToken("token");
        return logoutRequest;
    }
}