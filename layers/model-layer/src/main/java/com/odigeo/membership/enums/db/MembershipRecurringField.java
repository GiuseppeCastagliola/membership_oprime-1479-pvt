package com.odigeo.membership.enums.db;

import static com.odigeo.membership.enums.db.FieldName.TableAlias.MEMBERSHIP_RECURRING_ALIAS;

public enum MembershipRecurringField implements FieldName {
    ID,
    MEMBERSHIP_ID,
    RECURRING_ID,
    TIMESTAMP;

    @Override
    public String withTableAlias() {
        return MEMBERSHIP_RECURRING_ALIAS + "." + name();
    }

    @Override
    public String filteringName() {
        return withTableAlias();
    }
}
