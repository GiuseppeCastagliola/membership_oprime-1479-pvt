package com.odigeo.membership;

public enum UpdateMemberAccountOperation {
    UPDATE_NAMES, UPDATE_USER_ID
}
