package com.odigeo.membership;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MemberAccount implements Serializable {

    private final long id;
    private final long userId;
    private final String name;
    private final String lastNames;
    private List<Membership> memberships;
    private LocalDateTime timestamp;

    public MemberAccount(long id, long userId, String name, String lastNames) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.lastNames = lastNames;
        memberships = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public MemberAccount setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public MemberAccount setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MemberAccount) || getClass() != o.getClass()) {
            return false;
        }

        MemberAccount that = (MemberAccount) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).toHashCode();
    }
}
