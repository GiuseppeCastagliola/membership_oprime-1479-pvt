package com.odigeo.membership.activation.validation;

public abstract class AbstractValidation<O> implements Validation {

    protected O o;

    private AbstractValidation() {
        super();
    }

    public AbstractValidation(O o) {
        this.o = o;
    }

}
