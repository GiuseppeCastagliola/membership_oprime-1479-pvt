package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

import java.util.Objects;

public class BookingHasMembershipSubscriptionProductValidation extends AbstractValidation<BookingDetail> {

    private static final String MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE = "MEMBERSHIP_SUBSCRIPTION";

    public BookingHasMembershipSubscriptionProductValidation(BookingDetail o) {
        super(o);
    }

    @Override
    public boolean validate() {
        if (Objects.nonNull(o.getBookingProducts())) {
            return o.getBookingProducts().stream()
                    .anyMatch(productCategoryBooking -> Objects.nonNull(productCategoryBooking)
                            && MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE.equals(productCategoryBooking.getProductType()));
        }
        return Boolean.FALSE;
    }
}
