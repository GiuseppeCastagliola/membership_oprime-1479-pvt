package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.MembershipField;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.odigeo.membership.enums.db.Condition.GTE;
import static com.odigeo.membership.enums.db.Condition.LTE;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

public class MembershipSearch extends SearchQueries {

    private final String website;
    private final String status;
    private final String autoRenewal;
    private final String fromExpirationDate;
    private final String toExpirationDate;
    private final String fromActivationDate;
    private final String toActivationDate;
    private final String fromCreationDate;
    private final String toCreationDate;
    private final String membershipType;
    private final BigDecimal minBalance;
    private final BigDecimal maxBalance;
    private final Integer monthsDuration;
    private final String productStatus;
    private final BigDecimal totalPrice;
    private final BigDecimal renewalPrice;
    private final String currencyCode;
    private final String sourceType;
    private final Long memberAccountId;
    private final boolean withStatusActions;
    private final boolean withMemberAccount;
    private final MemberAccountSearch memberAccountSearch;
    private final String query;
    private final List<FilterCondition> filterConditions;

    public MembershipSearch(Builder builder) {
        website = builder.website;
        status = builder.status;
        autoRenewal = builder.autoRenewal;
        fromExpirationDate = builder.fromExpirationDate;
        toExpirationDate = builder.toExpirationDate;
        fromActivationDate = builder.fromActivationDate;
        toActivationDate = builder.toActivationDate;
        fromCreationDate = builder.fromCreationDate;
        toCreationDate = builder.toCreationDate;
        membershipType = builder.membershipType;
        minBalance = builder.minBalance;
        maxBalance = builder.maxBalance;
        productStatus = builder.productStatus;
        totalPrice = builder.totalPrice;
        renewalPrice = builder.renewalPrice;
        sourceType = builder.sourceType;
        monthsDuration = builder.monthsDuration;
        currencyCode = builder.currencyCode;
        memberAccountId = builder.memberAccountId;
        withStatusActions = builder.withStatusActions;
        withMemberAccount = builder.withMemberAccount;
        memberAccountSearch = builder.memberAccountSearch;
        filterConditions = Collections.unmodifiableList(toList());
        query = queryCompiler(filterConditions);
    }

    public boolean isWithStatusActions() {
        return withStatusActions;
    }

    public boolean isWithMemberAccount() {
        return withMemberAccount;
    }

    private List<FilterCondition> toList() {
        List<FilterCondition> conditions = new ArrayList<>();
        addEqualsIfNotNull(conditions, MembershipField.WEBSITE, website);
        addEqualsIfNotNull(conditions, MembershipField.STATUS, status);
        addEqualsIfNotNull(conditions, MembershipField.AUTO_RENEWAL, autoRenewal);
        addConditionIfNotNull(conditions, MembershipField.EXPIRATION_DATE, GTE, getStartOfDayTimestamp(fromExpirationDate));
        addConditionIfNotNull(conditions, MembershipField.EXPIRATION_DATE, LTE, getEndOfDayTimestamp(toExpirationDate));
        addConditionIfNotNull(conditions, MembershipField.ACTIVATION_DATE, GTE, getStartOfDayTimestamp(fromActivationDate));
        addConditionIfNotNull(conditions, MembershipField.ACTIVATION_DATE, LTE, getEndOfDayTimestamp(toActivationDate));
        addConditionIfNotNull(conditions, MembershipField.TIMESTAMP, GTE, getStartOfDayTimestamp(fromCreationDate));
        addConditionIfNotNull(conditions, MembershipField.TIMESTAMP, LTE, getEndOfDayTimestamp(toCreationDate));
        addEqualsIfNotNull(conditions, MembershipField.MEMBERSHIP_TYPE, membershipType);
        addConditionIfNotNull(conditions, MembershipField.BALANCE, GTE, minBalance);
        addConditionIfNotNull(conditions, MembershipField.BALANCE, LTE, maxBalance);
        addEqualsIfNotNull(conditions, MembershipField.PRODUCT_STATUS, productStatus);
        addEqualsIfNotNull(conditions, MembershipField.TOTAL_PRICE, totalPrice);
        addEqualsIfNotNull(conditions, MembershipField.RENEWAL_PRICE, renewalPrice);
        addEqualsIfNotNull(conditions, MembershipField.SOURCE_TYPE, sourceType);
        addEqualsIfNotNull(conditions, MembershipField.MONTHS_DURATION, monthsDuration);
        addEqualsIfNotNull(conditions, MembershipField.CURRENCY_CODE, currencyCode);
        addEqualsIfNotNull(conditions, MembershipField.MEMBER_ACCOUNT_ID, memberAccountId);
        return conditions;
    }

    private static Timestamp getEndOfDayTimestamp(String date) {
        Timestamp timestamp = null;
        if (nonNull(date)) {
            timestamp = Timestamp.valueOf(LocalDate.parse(date).atTime(LocalTime.MAX));
        }
        return timestamp;
    }

    private static Timestamp getStartOfDayTimestamp(String date) {
        Timestamp timestamp = null;
        if (nonNull(date)) {
            timestamp = Timestamp.valueOf(LocalDate.parse(date).atStartOfDay());
        }
        return timestamp;
    }

    public String getQueryString() {
        return query;
    }

    public static class Builder {
        private String website;
        private String status;
        private String autoRenewal;
        private String fromExpirationDate;
        private String toExpirationDate;
        private String fromActivationDate;
        private String toActivationDate;
        private String fromCreationDate;
        private String toCreationDate;
        private String membershipType;
        private BigDecimal minBalance;
        private BigDecimal maxBalance;
        private Integer monthsDuration;
        private String productStatus;
        private BigDecimal totalPrice;
        private BigDecimal renewalPrice;
        private String currencyCode;
        private String sourceType;
        private Long memberAccountId;
        private boolean withStatusActions;
        private boolean withMemberAccount;
        private MemberAccountSearch memberAccountSearch;

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder autoRenewal(String autoRenewal) {
            this.autoRenewal = autoRenewal;
            return this;
        }

        public Builder fromExpirationDate(String fromExpirationDate) {
            this.fromExpirationDate = fromExpirationDate;
            return this;
        }

        public Builder toExpirationDate(String toExpirationDate) {
            this.toExpirationDate = toExpirationDate;
            return this;
        }

        public Builder fromActivationDate(String fromActivationDate) {
            this.fromActivationDate = fromActivationDate;
            return this;
        }

        public Builder toActivationDate(String toActivationDate) {
            this.toActivationDate = toActivationDate;
            return this;
        }

        public Builder fromCreationDate(String fromCreationDate) {
            this.fromCreationDate = fromCreationDate;
            return this;
        }

        public Builder toCreationDate(String toCreationDate) {
            this.toCreationDate = toCreationDate;
            return this;
        }

        public Builder membershipType(String membershipType) {
            this.membershipType = membershipType;
            return this;
        }

        public Builder monthsDuration(Integer monthsDuration) {
            this.monthsDuration = monthsDuration;
            return this;
        }

        public Builder productStatus(String productStatus) {
            this.productStatus = productStatus;
            return this;
        }

        public Builder totalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public Builder renewalPrice(BigDecimal renewalPrice) {
            this.renewalPrice = renewalPrice;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder sourceType(String sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        public Builder withStatusActions(boolean withStatusActions) {
            this.withStatusActions = withStatusActions;
            return this;
        }

        public Builder minBalance(BigDecimal minBalance) {
            this.minBalance = minBalance;
            return this;
        }

        public Builder maxBalance(BigDecimal maxBalance) {
            this.maxBalance = maxBalance;
            return this;
        }

        public Builder memberAccountId(Long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public Builder withMemberAccount(boolean withMemberAccount) {
            this.withMemberAccount = withMemberAccount;
            return this;
        }

        public Builder memberAccountSearch(MemberAccountSearch memberAccountSearch) {
            this.memberAccountSearch = memberAccountSearch;
            return this;
        }

        public MembershipSearch build() {
            return new MembershipSearch(this);
        }
    }

    private String queryCompiler(List<FilterCondition> filterConditions) {
        String queryString;
        if (withMemberAccount) {
            queryString = withStatusActions ? SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS_AND_ACCOUNT : SEARCH_MEMBERSHIPS_WITH_ACCOUNT;
        } else {
            queryString = withStatusActions ? SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS : SEARCH_MEMBERSHIPS;
        }
        StringBuilder queryBuilder = new StringBuilder(queryString);
        queryBuilder.append(SqlKeywords.WHERE)
                .append(filterConditions.stream()
                        .map(FilterCondition::toString)
                        .collect(joining(SqlKeywords.AND.toString())));
        if (withMemberAccount && nonNull(memberAccountSearch)) {
            if (CollectionUtils.isNotEmpty(filterConditions)) {
                queryBuilder.append(SqlKeywords.AND);
            }
            queryBuilder.append(memberAccountSearch.getFilterConditions().stream()
                    .map(FilterCondition::toString)
                    .collect(joining(SqlKeywords.AND.toString())));
        }
        return queryBuilder.toString();
    }

    public List<Object> getValues() {
        List<Object> objects = filterConditions.stream()
                .map(FilterCondition::getValue)
                .collect(Collectors.toList());
        if (withMemberAccount && nonNull(memberAccountSearch)) {
            objects.addAll(memberAccountSearch.getValues());
        }
        return objects;
    }

    @Override
    public boolean isEmpty() {
        Object[] fieldArray = {website, status, autoRenewal, fromExpirationDate, toExpirationDate, fromActivationDate, toActivationDate,
            fromCreationDate, toCreationDate, membershipType, minBalance, maxBalance, monthsDuration, productStatus,
            totalPrice, renewalPrice, currencyCode, sourceType, memberAccountId};
        boolean thisIsEmpty = Arrays.stream(fieldArray).noneMatch(Objects::nonNull);
        if (thisIsEmpty && nonNull(memberAccountSearch)) {
            return memberAccountSearch.isEmpty();
        }
        return thisIsEmpty;
    }
}
