package com.odigeo.membership.enums.db;

public enum Condition {
    EQ(" = ? "), LT(" < ? "), GT(" > ? "), LTE(" <= ? "), GTE(" >= ? ");

    private final String queryChunk;

    Condition(String queryChunk) {
        this.queryChunk = queryChunk;
    }

    public String getQueryChunk() {
        return queryChunk;
    }
}
