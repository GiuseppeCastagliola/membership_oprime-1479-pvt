package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.memberapi.v1.MembershipServiceController;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.util.StringUtils;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.spi.HttpRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

public class InterceptorHelperTest {
    private static String ENABLE_AUTORENEWAL_MEMBERSHIP_URI = "http://localhost/membership/membership/v1/disable-auto-renewal/22";
    private static String EXAMPLE_MEMBERSHIP_URI = "http://localhost/membership/membership/v1/a/b/c";

    private InterceptorHelper interceptorHelper;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(StringUtils.class).toInstance(new StringUtils()));
        interceptorHelper = new InterceptorHelper();
    }

    @Test
    public void testBuildOneLineString() throws UnsupportedEncodingException {
        String line = interceptorHelper.buildOneLineString("\t A \nB \t C \n".getBytes());
        assertEquals(line, "  A  B   C  ");
    }

    @Test
    public void testIsRequestOf() throws NoSuchMethodException {
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("disableAutoRenewal", Long.class);
        assertTrue(interceptorHelper.isRequestOf(disableAutoRenewalMethod, "disable-auto-renewal/{id}"));
    }

    @Test
    public void testExtractFieldFromBody() {
        String body = "{\n" +
            "  \"membershipId\": \"76754\",\n" +
            "  \"operation\": \"DEACTIVATE_MEMBERSHIP\"\n" +
            "}";

        Optional<String> membershipId = interceptorHelper.extractFieldFromBody(body, UpdateMembershipRequest::getMembershipId);
        Optional<String> operation = interceptorHelper.extractFieldFromBody(body, UpdateMembershipRequest::getOperation);
        Optional<String> membershipStatus = interceptorHelper.extractFieldFromBody(body, UpdateMembershipRequest::getMembershipStatus);

        assertTrue(membershipId.isPresent());
        assertTrue(operation.isPresent());
        assertFalse(membershipStatus.isPresent());
        assertEquals(membershipId.get(), "76754");
        assertEquals(operation.get(), "DEACTIVATE_MEMBERSHIP");
    }

    @Test
    public void testExtractSegmentFromPath() throws URISyntaxException {
        HttpRequest httpRequestMock = MockHttpRequest.put(EXAMPLE_MEMBERSHIP_URI);

        Optional<String> segment = interceptorHelper.extractSegmentFromPath(httpRequestMock, 3);

        assertTrue(segment.isPresent());
        assertEquals(segment.get(), "a");
    }

    @Test
    public void testExtractLastSegmentFromPath() throws URISyntaxException {
        HttpRequest httpRequestMock = MockHttpRequest.put(EXAMPLE_MEMBERSHIP_URI);

        Optional<String> segment = interceptorHelper.extractSegmentFromPath(httpRequestMock, 1);

        assertTrue(segment.isPresent());
        assertEquals(segment.get(), "c");
    }

    @Test
    public void testExtractInvalidSegmentFromPath() throws URISyntaxException {
        HttpRequest httpRequestMock = MockHttpRequest.put(EXAMPLE_MEMBERSHIP_URI);
        assertFalse(interceptorHelper.extractSegmentFromPath(httpRequestMock, 7).isPresent());
    }

    @Test
    public void testExtractClientModuleFromHeader() throws URISyntaxException {
        HttpRequest httpRequestMock = MockHttpRequest
            .put(ENABLE_AUTORENEWAL_MEMBERSHIP_URI)
            .header("odigeo-module-info", "some-module:v1");
        assertEquals(interceptorHelper.extractClientModuleFromHeader(httpRequestMock), "some-module");
    }
}