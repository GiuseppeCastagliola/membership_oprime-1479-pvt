package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.member.BackOfficeService;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipBackOfficeServiceControllerTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final String EMAIL = "test@edreams.com";
    private static final DataAccessException DATA_ACCESS_EXCEPTION = new DataAccessException(StringUtils.EMPTY);
    private static final MissingElementException MISSING_ELEMENT_EXCEPTION = new MissingElementException(StringUtils.EMPTY);

    @Mock
    private BackOfficeService backOfficeService;

    private MembershipBackOfficeServiceController membershipBackOfficeServiceController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipBackOfficeServiceController = new MembershipBackOfficeServiceController();
    }

    private void configure(Binder binder) {
        binder.bind(BackOfficeService.class).toInstance(backOfficeService);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMessageSent()
        throws InvalidParametersException, MissingElementException, DataAccessException {
        // Given
        when(backOfficeService.updateMembershipMarketingInfo(MEMBERSHIP_ID)).thenReturn(Boolean.TRUE);
        // When
        Boolean result = membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL);
        // Then
        assertTrue(result);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMessageNotSent()
        throws InvalidParametersException, MissingElementException, DataAccessException {
        // Given
        when(backOfficeService.updateMembershipMarketingInfo(MEMBERSHIP_ID)).thenReturn(Boolean.FALSE);
        // When
        Boolean result = membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL);
        // Then
        assertFalse(result);
    }

    @Test
    public void testUpdateMembershipMarketingInfoReturnsFalseWhenMissingElementException()
        throws MissingElementException, DataAccessException, InvalidParametersException {
        // Given
        when(backOfficeService.updateMembershipMarketingInfo(MEMBERSHIP_ID)).thenThrow(MISSING_ELEMENT_EXCEPTION);
        // When
        Boolean result = membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL);
        // Then
        assertFalse(result);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testUpdateMembershipMarketingInfoReturnsFalseWhenDataAccessException()
        throws MissingElementException, DataAccessException, InvalidParametersException {
        // Given
        when(backOfficeService.updateMembershipMarketingInfo(MEMBERSHIP_ID)).thenThrow(DATA_ACCESS_EXCEPTION);
        // When
        membershipBackOfficeServiceController.updateMembershipMarketingInfo(MEMBERSHIP_ID, EMAIL);
    }
}
