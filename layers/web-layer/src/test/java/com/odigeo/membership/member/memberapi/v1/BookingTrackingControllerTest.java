package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.tracking.BookingTrackingService;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class BookingTrackingControllerTest {

    @Mock
    private BookingTrackingService bookingTrackingService;

    @InjectMocks
    private BookingTrackingController bookingTrackingController = new BookingTrackingController();

    public static final Long BOOKING_ID = 10L;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            protected void configure() {
                bind(BookingTrackingService.class).toInstance(bookingTrackingService);
            }
        });
    }

    @Test(expectedExceptions = InvalidParametersException.class)
    public void testRetryBookingTrackingNullBookingId() throws InvalidParametersException {
        bookingTrackingController.retryBookingTracking(null);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testRetryBookingTrackingProcessBookingError()
            throws InvalidParametersException, MissingElementException, RetryableException {
        doThrow(new MissingElementException(StringUtils.EMPTY)).when(bookingTrackingService).processBooking(anyLong());
        bookingTrackingController.retryBookingTracking(BOOKING_ID);
    }

    @Test
    public void testRetryBookingTrackingOK() throws InvalidParametersException {
        assertTrue(bookingTrackingController.retryBookingTracking(BOOKING_ID));
    }

}