package com.odigeo.membership.member.bootstrap.betatracking.filters;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.trackingapi.client.v2.context.FlowContextManager;
import com.odigeo.trackingapi.client.v2.context.TrackingExecutionContextFilter;

import javax.servlet.ServletRequest;
import java.util.UUID;

public class MembershipTrackingExecutionContextFilter extends TrackingExecutionContextFilter {

    private static final String MEMBERSHIP_FLOW_NAME = "membershipFlow";

    @Override
    protected String getFlowName(ServletRequest servletRequest) {
        return UUID.randomUUID().toString();
    }

    @Override
    protected String getFlowNamespace() {
        return MEMBERSHIP_FLOW_NAME;
    }

    @Override
    protected FlowContextManager getFlowContextManager() {
        return ConfigurationEngine.getInstance(FlowContextManager.class);
    }
}
