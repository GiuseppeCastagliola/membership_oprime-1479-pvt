package com.odigeo.membership.member.memberapi.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.Optional;

public final class LogUtils {

    private LogUtils() {
    }

    public static void smallLog(Logger logger, String message, Exception e) {
        logger.error("{}\n\t{}", message, e.getMessage());
    }

    public static void defaultLoggerError(Logger logger, Exception exception, String suffix) {
        String methodName = Optional.ofNullable(exception.getStackTrace())
                .map(elements -> elements[0].getMethodName())
                .orElse(StringUtils.EMPTY);
        logger.error("{} : {} {}", exception.getClass().getSimpleName(), methodName, suffix, exception);
    }

    public static void defaultLoggerError(Logger logger, Exception e) {
        defaultLoggerError(logger, e, StringUtils.EMPTY);
    }
}
