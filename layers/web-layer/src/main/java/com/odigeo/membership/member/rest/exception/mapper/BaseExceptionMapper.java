package com.odigeo.membership.member.rest.exception.mapper;

import com.odigeo.membership.member.rest.utils.BaseExceptionBean;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by roc.arajol on 01-Feb-18.
 */
public abstract class BaseExceptionMapper<T extends Exception> {

    public final Response toResponse(T exception) {

        BaseExceptionBean exceptionBean = beanToSend(exception);
        return Response.status(exceptionBean.getStatusToSend())
                .type(mediaTypeToSend())
                .entity(exceptionBean).build();
    }

    /**
     * Implement this to control the bean information sent in the response for this exception
     */

    protected abstract BaseExceptionBean beanToSend(T exception);


    /**
     * Override this to control the Content-Type of the response for this exception
     */
    protected String mediaTypeToSend() {
        return MediaType.APPLICATION_JSON;
    }
}
