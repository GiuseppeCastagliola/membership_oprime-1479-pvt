package com.odigeo.membership.member.rest.utils;

import com.odigeo.commons.rest.error.SimpleExceptionBean;

import javax.ws.rs.core.Response;


public abstract class BaseExceptionBean extends SimpleExceptionBean {

    BaseExceptionBean(Throwable throwable, boolean includeCause) {
        super(throwable, includeCause);
    }

    public abstract Response.Status getStatusToSend();
}
