package com.odigeo.membership.member.memberapi.v1;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipPropertiesConfigService;
import com.odigeo.membership.propertiesconfig.PropertiesConfigurationService;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class MembershipPropertiesConfigController implements MembershipPropertiesConfigService {

    @Override
    public Boolean enableConfigurationProperty(final String propertyKey) {
        return getPropertiesConfigurationService().updatePropertyValue(propertyKey, TRUE);
    }

    @Override
    public Boolean disableConfigurationProperty(final String propertyKey) {
        return getPropertiesConfigurationService().updatePropertyValue(propertyKey, FALSE);
    }

    private PropertiesConfigurationService getPropertiesConfigurationService() {
        return ConfigurationEngine.getInstance(PropertiesConfigurationService.class);
    }
}
