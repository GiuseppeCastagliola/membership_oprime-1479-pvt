package com.odigeo.membership.member.bootstrap;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;
import com.odigeo.visitengineapi.last.VisitEngine;

public class VisitEngineModule extends AbstractRestUtilsModule {

    public VisitEngineModule(ServiceNotificator... serviceNotificators) {
        super(VisitEngine.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpDefault(VisitEngine.class);
    }
}
