package com.odigeo.membership.member.rest.exception.mapper;

import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.member.rest.utils.MembershipServiceExceptionBean;

import javax.validation.ValidationException;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ValidationExceptionMapper extends BaseExceptionMapper<ValidationException> implements ExceptionMapper<ValidationException> {

    @Override
    protected MembershipServiceExceptionBean beanToSend(ValidationException exception) {
        return new MembershipServiceExceptionBean(MembershipExceptionMapper.map(exception), true);
    }
}
