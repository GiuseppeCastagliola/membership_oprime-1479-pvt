package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.enums.db.MembershipField;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Singleton
public class MemberAccountEntityBuilder {

    private static final String ACCOUNT_ID = "ACCOUNT_ID";
    private static final String USER_ID = "USER_ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String TIMESTAMP = "TIMESTAMP";

    public MemberAccount build(ResultSet rs, String idColumnLabel) throws SQLException {
        long id = rs.getLong(idColumnLabel);
        long userId = rs.getLong(USER_ID);
        String name = rs.getString(FIRST_NAME);
        String lastNames = rs.getString(LAST_NAME);
        Timestamp timestamp = rs.getTimestamp(TIMESTAMP);
        return new MemberAccount(id, userId, name, lastNames).setTimestamp(timestamp.toLocalDateTime());
    }

    public List<MemberAccount> buildWithMembership(ResultSet rs) throws SQLException {
        Map<Long, MemberAccount> memberAccountMap = new LinkedHashMap<>();
        Map<Long, Membership> membershipMap = new LinkedHashMap<>();
        while (rs.next()) {
            MemberAccount memberAccount = build(rs, ACCOUNT_ID);
            memberAccountMap.putIfAbsent(memberAccount.getId(), memberAccount);
            Long membershipId = rs.getLong(MembershipField.ID.name());
            membershipMap.putIfAbsent(membershipId, getMembershipEntityBuilder().build(rs, false));
            Membership membership = membershipMap.get(membershipId);
            extractRecurringFromResultSetToMembership(rs, membership);
        }
        return createMemberAccountWithMemberships(memberAccountMap, membershipMap);
    }

    private List<MemberAccount> createMemberAccountWithMemberships(Map<Long, MemberAccount> memberAccountMap, Map<Long, Membership> membershipMap) {
        return memberAccountMap.values()
                                .stream()
                                .map(memberAccount -> memberAccount.setMemberships(membershipMap.values().stream()
                                        .filter(membership -> Objects.equals(memberAccount.getId(), membership.getMemberAccountId()))
                                        .collect(Collectors.toList())))
                                .collect(Collectors.toList());

    }

    private void extractRecurringFromResultSetToMembership(ResultSet resultSet, Membership membership) throws SQLException {
        getMembershipRecurringEntityBuilder().build(resultSet)
                .ifPresent(membership.getMembershipRecurring()::add);
    }

    private MembershipEntityBuilder getMembershipEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipEntityBuilder.class);
    }

    private MembershipRecurringEntityBuilder getMembershipRecurringEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipRecurringEntityBuilder.class);
    }
}
