package com.odigeo.membership.member.util;

import com.google.inject.Singleton;
import com.odigeo.membership.MembershipRecurring;
import com.odigeo.membership.enums.db.MembershipRecurringField;
import org.apache.commons.lang.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Singleton
public class MembershipRecurringEntityBuilder {

    private static final String RECURRING_ID_COLUMN_NAME = MembershipRecurringField.RECURRING_ID.name();

    private MembershipRecurring createMembershipRecurring(String recurringId) {
        return MembershipRecurring.builder()
                .recurringId(recurringId)
                .build();
    }

    public Optional<MembershipRecurring> build(ResultSet rs) throws SQLException {
        return Optional.ofNullable(rs.getString(RECURRING_ID_COLUMN_NAME))
                .filter(StringUtils::isNotBlank)
                .map(recurringId -> Optional.of(createMembershipRecurring(recurringId)))
                .orElse(Optional.empty());
    }

}
