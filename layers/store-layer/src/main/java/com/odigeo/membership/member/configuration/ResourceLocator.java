package com.odigeo.membership.member.configuration;

import com.odigeo.membership.exception.UnavailableDataSourceException;

import javax.sql.DataSource;

public interface ResourceLocator {

    DataSource getDefaultDataSource() throws UnavailableDataSourceException;
}
