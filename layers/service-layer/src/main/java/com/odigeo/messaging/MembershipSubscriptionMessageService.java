package com.odigeo.messaging;

import com.google.inject.Inject;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public class MembershipSubscriptionMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipSubscriptionMessageService.class);

    private final UserApiManager userApiManager;

    @Inject
    MembershipSubscriptionMessageService(UserApiManager userApiManager) {
        this.userApiManager = userApiManager;
    }

    public MembershipSubscriptionMessage completeSubscriptionMessage(MembershipSubscriptionMessage message) {
        MembershipSubscriptionMessage.Builder messageBuilder = new MembershipSubscriptionMessage
                .Builder(message.getEmail(), message.getWebsite())
                .withDates(message.getActivationDate(), message.getExpirationDate())
                .withSubscriptionStatus(message.getSubscriptionStatus());
        addUserInfo(message.getEmail(), message.getWebsite(), messageBuilder);

        return messageBuilder.build();
    }

    private void addUserInfo(String email, String website, MembershipSubscriptionMessage.Builder messageBuilder) {
        UserInfo userInfo = userApiManager.getUserInfo(email, website);
        messageBuilder.withShouldSetPassword(userInfo.shouldSetPassword());
        addForgetPasswordTokenToSubscriptionMessage(userInfo, messageBuilder);
    }

    public MembershipSubscriptionMessage getMembershipSubscriptionMessage(Membership membership,
                                                                          SubscriptionStatus subscriptionStatus) throws DataNotFoundException {
        long userId = Optional.ofNullable(membership.getMemberAccount()).map(MemberAccount::getUserId)
                .orElseThrow(() -> new DataNotFoundException("Can't generate subscription message for membershipId " + membership.getId() + ": MemberAccount is null"));
        UserInfo userInfo = userApiManager.getUserInfo(userId);
        return buildMembershipSubscriptionMessage(userInfo, membership, subscriptionStatus);
    }

    MembershipSubscriptionMessage buildMembershipSubscriptionMessage(UserInfo userInfo, Membership membership,
                                                                             SubscriptionStatus subscriptionStatus) throws DataNotFoundException {
        String email = Optional.ofNullable(userInfo.getEmail())
                .orElseThrow(() -> new DataNotFoundException("User api returned email null for membershipId " + membership.getId()));
        LocalDate activationDate = Optional.ofNullable(membership.getActivationDate()).map(LocalDateTime::toLocalDate)
                .orElse(membership.getExpirationDate().toLocalDate().minusMonths(membership.getMonthsDuration()));
        MembershipSubscriptionMessage.Builder subscriptionMessageBuilder = new MembershipSubscriptionMessage.Builder(email, membership.getWebsite())
                .withDates(activationDate, membership.getExpirationDate().toLocalDate())
                .withSubscriptionStatus(subscriptionStatus)
                .withShouldSetPassword(userInfo.shouldSetPassword());

        addForgetPasswordTokenToSubscriptionMessage(userInfo, subscriptionMessageBuilder);
        return subscriptionMessageBuilder.build();
    }

    private void addForgetPasswordTokenToSubscriptionMessage(UserInfo userInfo, MembershipSubscriptionMessage.Builder messageBuilder) {
        if (userInfo.getUserId().isPresent()) {
            if (userInfo.shouldSetPassword()) {
                final Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(userInfo.getUserId().get());
                if (forgetPasswordToken.isPresent()) {
                    messageBuilder.withForgetPasswordToken(forgetPasswordToken.get());
                } else {
                    messageBuilder.withShouldSetPassword(false);
                    LOGGER.error("Error: expected ForgetPasswordToken but retrieved null for userId: {}", userInfo.getUserId());
                }
            }
        } else {
            LOGGER.warn("Error: no user basic info for userId: {}", userInfo.getUserId());
        }
    }
}
