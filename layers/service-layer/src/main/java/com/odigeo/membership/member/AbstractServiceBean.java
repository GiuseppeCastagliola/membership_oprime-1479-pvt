package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.configuration.JeeResourceLocator;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Objects;

public abstract class AbstractServiceBean {

    @Resource(mappedName = JeeResourceLocator.DEFAULT_DATASOURCE_NAME)
    protected DataSource dataSource;

    private MemberManager memberManager;

    protected MemberManager getMemberManager() {
        if (Objects.isNull(memberManager)) {
            memberManager = ConfigurationEngine.getInstance(MemberManager.class);
        }
        return memberManager;
    }
}
