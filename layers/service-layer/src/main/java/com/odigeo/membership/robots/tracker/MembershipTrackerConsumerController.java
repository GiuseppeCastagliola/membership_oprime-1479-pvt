package com.odigeo.membership.robots.tracker;

import com.google.inject.Inject;
import com.odigeo.membership.robots.ConsumerController;
import com.odigeo.membership.robots.activator.BookingUpdatesConsumerConfiguration;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MembershipTrackerConsumerController implements ConsumerController {

    private final Consumer<BasicMessage> consumer;
    private final int numThreads;
    private final ExecutorService executorService;

    @Inject
    public MembershipTrackerConsumerController(BookingUpdatesConsumerConfiguration configuration) {
        consumer = new KafkaJsonConsumer<>(BasicMessage.class, configuration.getZooKeeperUrl(), configuration.getTrackerConsumerGroup(), configuration.getTopicName());
        numThreads = configuration.getNumConsumerThreads();
        executorService = Executors.newFixedThreadPool(numThreads);
    }

    @Override
    public void startMultiThreadConsumer() {
        List<ConsumerIterator<BasicMessage>> consumerIteratorList = consumer.connectAndIterate(numThreads);
        for (ConsumerIterator<BasicMessage> consumerIterator : consumerIteratorList) {
            executorService.execute(new MembershipTrackerConsumerWorker(consumerIterator));
        }
    }

    @Override
    public void stopMultiThreadConsumer() {
        consumer.close();
        executorService.shutdown();
    }

    public void join() throws InterruptedException {
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
    }

}
