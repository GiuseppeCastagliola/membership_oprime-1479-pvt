package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;

public interface BackOfficeService {

    boolean updateMembershipMarketingInfo(Long membershipId) throws MissingElementException, DataAccessException;

    boolean sendWelcomeEmail(Long membershipId, Long bookingId) throws MissingElementException, DataAccessException;
}
