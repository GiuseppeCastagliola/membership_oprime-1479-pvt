package com.odigeo.membership.robots.utils;

import com.odigeo.membership.exception.RetryableException;
import net.jodah.failsafe.RetryPolicy;

import java.time.temporal.ChronoUnit;

public final class RobotUtils {

    private static final int RETRY_DELAY = 5;
    private static final int RETRY_DELAY_EXP_FACTOR = 3;
    private static final double JITTER_FACTOR = 0.2;
    private static final int MAX_RETRY_DELAY = 300;
    private static final int RETRIES = 6;
    public static final RetryPolicy<Object> RETRY_POLICY = new RetryPolicy<>()
            .handle(RetryableException.class)
            .withBackoff(RETRY_DELAY, MAX_RETRY_DELAY, ChronoUnit.MINUTES, RETRY_DELAY_EXP_FACTOR)
            .withJitter(JITTER_FACTOR)
            .withMaxRetries(RETRIES);

    private RobotUtils() {
    }
}
