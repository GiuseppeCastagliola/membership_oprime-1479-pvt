package com.odigeo.membership.exception;

public class ActivatedMembershipException extends Exception {

    public ActivatedMembershipException(String message) {
        super(message);
    }
}
