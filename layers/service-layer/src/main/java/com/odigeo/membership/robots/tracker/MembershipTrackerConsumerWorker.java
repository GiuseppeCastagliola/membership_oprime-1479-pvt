package com.odigeo.membership.robots.tracker;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import net.jodah.failsafe.Failsafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.odigeo.membership.robots.utils.RobotUtils.RETRY_POLICY;

public class MembershipTrackerConsumerWorker implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipTrackerConsumerWorker.class);
    private static final int CORE_POOL_SIZE = 2;

    private final ConsumerIterator<BasicMessage> consumerMessageIterator;
    private final ScheduledExecutorService executor;

    public MembershipTrackerConsumerWorker(ConsumerIterator consumerMessageIterator) {
        this.consumerMessageIterator = consumerMessageIterator;
        executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
    }

    @Override
    public void run() {
        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();
        try {
            while (consumerMessageIterator.hasNext()) {
                BasicMessage bookingMessage = consumerMessageIterator.next();
                String bookingId = bookingMessage.getKey();
                LOGGER.info("[Read from consumer, booking #{}]", bookingId);
                RETRY_POLICY.onRetry(ctx -> {
                    LOGGER.info("Retry #{} for bookingId {} ", ctx.getAttemptCount(), bookingId);
                    MetricsUtils.incrementCounter(MetricsBuilder.buildTrackingRetryAttemptNumberMetric(MetricsNames.TRACKING_ATTEMPTS, ctx.getAttemptCount()), MetricsNames.METRICS_REGISTRY_NAME);
                });
                completableFutureList.add(Failsafe.with(RETRY_POLICY)
                        .with(executor)
                        .onFailure(failure -> {
                            LOGGER.warn("Booking {} will not be tracked after {} attempts, last error: {} ", bookingId, failure.getAttemptCount(), failure.getFailure().getMessage());
                            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKING_FAILURES), MetricsNames.METRICS_REGISTRY_NAME);
                        })
                        .runAsync(() -> getBookingTrackingService().processBooking(Long.parseLong(bookingId))));
            }
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error accessing Kafka", e);
        } catch (MessageParserException e) {
            LOGGER.error("Error parsing message from Kafka before tracking", e);
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[completableFutureList.size()])).join();
    }

    private BookingTrackingService getBookingTrackingService() {
        return ConfigurationEngine.getInstance(BookingTrackingService.class);
    }
}
