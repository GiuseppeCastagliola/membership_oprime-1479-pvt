package com.odigeo.membership.robots;

public interface ConsumerController {

    void startMultiThreadConsumer();

    void stopMultiThreadConsumer();

    void join() throws InterruptedException;
}
