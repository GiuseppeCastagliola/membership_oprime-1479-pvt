package com.odigeo.membership.member.userarea;

import static org.apache.commons.lang.StringUtils.length;
import static org.apache.commons.lang.StringUtils.overlay;
import static org.apache.commons.lang.StringUtils.repeat;

final class CreditCardOperation {

    private static final String CC_NUMBER_MASK = "*";
    private static final int VISIBLE_CC_NUMBERS = 4;

    private CreditCardOperation() {

    }

    static String maskCreditCardNumber(final String creditCardNumber) {
        int maskLength = length(creditCardNumber) - VISIBLE_CC_NUMBERS;
        String maskedNumber = repeat(CC_NUMBER_MASK, maskLength);
        return overlay(creditCardNumber, maskedNumber, 0, maskLength);
    }
}
