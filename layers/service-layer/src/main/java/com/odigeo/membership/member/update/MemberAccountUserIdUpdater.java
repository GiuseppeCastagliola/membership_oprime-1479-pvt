package com.odigeo.membership.member.update;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.AuditMemberAccountManager;
import com.odigeo.membership.member.MemberAccountStore;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import static java.util.Objects.isNull;

@Singleton
public class MemberAccountUserIdUpdater {

    private final MemberAccountStore memberAccountStore;
    private final AuditMemberAccountManager auditMemberAccountManager;

    @Inject
    public MemberAccountUserIdUpdater(MemberAccountStore memberAccountStore,
                                      AuditMemberAccountManager auditMemberAccountManager) {
        this.memberAccountStore = memberAccountStore;
        this.auditMemberAccountManager = auditMemberAccountManager;
    }

    public boolean updateMemberAccountUserId(DataSource dataSource, UpdateMemberAccount updateMemberAccount) throws DataAccessException {
        long memberAccountId = Long.parseLong(updateMemberAccount.getMemberAccountId());
        long userId = updateMemberAccount.getUserId();
        try {
            boolean successfullyUpdated = memberAccountStore.updateMemberAccountUserId(dataSource, memberAccountId, userId);
            if (successfullyUpdated) {
                auditMemberAccountManager.auditUpdatedMemberAccount(dataSource, memberAccountId);
            }
            return successfullyUpdated;
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error trying to update userId " + userId + " with memberAccountId " + memberAccountId, e);
        }
    }

    public void validateParametersForUpdateUserIdOperation(UpdateMemberAccount updateMemberAccount) {
        if (isNull(updateMemberAccount.getMemberAccountId()) || isNull(updateMemberAccount.getUserId())) {
            throw new InvalidParameterException("MemberAccountId and UserId must be informed to update the userId");
        }
    }
}
