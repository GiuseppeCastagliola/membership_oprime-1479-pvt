package com.odigeo.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.util.EncryptUtils;
import com.odigeo.crm.CrmCipherConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CrmCipherTest {
    private static final String MESSAGE = "Secret message";
    private static final String SECRET_KEY = "SecKey00";
    private static final String RANDOM_TOKEN = "SW52YWxpZFRva2Vu";

    @Mock
    private CrmCipherConfiguration crmCipherConfiguration;

    private EncryptUtils encryptUtils = new EncryptUtils();
    private CrmCipher crmCipher;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(this::configure);
        when(crmCipherConfiguration.getSecretKey()).thenReturn(SECRET_KEY);
        crmCipher = ConfigurationEngine.getInstance(CrmCipher.class);
    }

    private void configure(Binder binder) {
        binder.bind(CrmCipherConfiguration.class).toInstance(crmCipherConfiguration);
        binder.bind(EncryptUtils.class).toInstance(encryptUtils);
    }

    @Test
    public void testCrmCipher() throws Exception {
        String encryptedMsg = crmCipher.encryptToken(MESSAGE);
        String decryptedMsg = crmCipher.decryptToken(encryptedMsg);
        assertEquals(decryptedMsg, MESSAGE);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testCrmCipherWithInvalidTokenShouldThrowException() throws Exception {
        crmCipher.decryptToken(RANDOM_TOKEN);
    }

}
