package com.odigeo.membership.member.userarea;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CreditCardOperationTest {

    @Test
    public void testMaskNullCreditCardReturnsEmpty() {
        //When
        String maskedCreditCard = CreditCardOperation.maskCreditCardNumber(null);
        //Then
        assertNull(maskedCreditCard);
    }

    @Test
    public void testMaskReturnsNothingMaskedWhenCreditCardNumberHasLessThanFourChars() {
        //Given
        String creditCardNumber = "1234";
        //When
        String maskedCreditCard = CreditCardOperation.maskCreditCardNumber(creditCardNumber);
        //Then
        assertEquals(maskedCreditCard, creditCardNumber);
    }

    @Test
    public void testMaskedCreditCardNumberWhenLengthBiggerThanFourChars() {
        //Given
        String creditCardNumber = "12345678";
        //When
        String maskedCreditCard = CreditCardOperation.maskCreditCardNumber(creditCardNumber);
        //Then
        assertEquals(maskedCreditCard, "****5678");
    }
}