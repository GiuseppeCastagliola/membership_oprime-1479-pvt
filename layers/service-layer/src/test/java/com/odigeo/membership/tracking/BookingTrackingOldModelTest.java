package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.Fee;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.booking.MembershipVatModelFee;
import com.odigeo.membership.exception.RetryableException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingTrackingOldModelTest {

    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 101L;
    private static final long FEE_CONTAINER_ID = 999L;
    private static final String CONTRACT = "CONTRACT";
    private static final String SITE_ES = "ES";
    private static final String BRAND_ED = "ED";
    private static final Random RANDOM = new Random();
    private static final BigDecimal AVOID_FEE_AMOUNT = new BigDecimal(-10);
    private static final BigDecimal PERKS_AMOUNT = new BigDecimal(-20);
    private static final BigDecimal CHANGED_AVOID_AMOUNT = new BigDecimal(-15);

    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private BookingTrackingStore bookingTrackingStore;

    private Membership membership;
    private BookingDetail bookingDetail;
    private BookingDetailWrapper bookingDetailWrapper;
    private AbstractBookingTracking bookingTrackingOldModel;

    @BeforeMethod
    public void setUp() throws BuilderException, SQLException {
        initMocks(this);
        membership = new MembershipBuilder().setId(MEMBERSHIP_ID).build();
        bookingDetail = buildBookingDetail();
        bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
        bookingTrackingOldModel = new BookingTrackingOldModel(membership, bookingDetailWrapper);
        ConfigurationEngine.init(this::configure);
        when(dataSource.getConnection()).thenReturn(connection);
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingStore.class).toInstance(bookingTrackingStore);
    }

    @Test
    public void testGetBookingTracking() throws RetryableException, BuilderException {
        // Given
        setUpBookingMembershipFees();

        // When
        Optional<BookingTracking> optionalBookingTracking = bookingTrackingOldModel.getBookingTracking();

        // Then
        assertTrue(optionalBookingTracking.isPresent());
        BookingTracking bookingTracking = optionalBookingTracking.get();
        assertEquals(bookingTracking.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingTracking.getBookingId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingTracking.getAvoidFeeAmount(), AVOID_FEE_AMOUNT);
        assertEquals(bookingTracking.getCostFeeAmount(), BigDecimal.ZERO);
        assertEquals(bookingTracking.getPerksAmount(), PERKS_AMOUNT);
    }

    @Test
    public void testDeleteTrackedBookingAndRestoreBalance() throws RetryableException, SQLException {
        // When
        bookingTrackingOldModel.deleteTrackedBookingAndRestoreBalance(dataSource);

        // Then
        verify(bookingTrackingStore).removeTrackedBooking(eq(dataSource), eq(BOOKING_ID));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testDeleteTrackedBookingAndRestoreBalanceSQLException() throws RetryableException, SQLException {
        // Given
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(BOOKING_ID))).thenThrow(new SQLException());

        // When
        bookingTrackingOldModel.deleteTrackedBookingAndRestoreBalance(dataSource);
    }

    @Test
    public void testAreFeesAmountChangedSameFees() throws BuilderException {
        // Given
        BookingTracking alreadyTrackedBooking = getAlreadyTrackedBooking(AVOID_FEE_AMOUNT, PERKS_AMOUNT);
        setUpBookingMembershipFees();

        // When
        boolean feesAmountChanged = bookingTrackingOldModel.areFeesAmountChanged(alreadyTrackedBooking);

        // Then
        assertFalse(feesAmountChanged);
    }

    @Test
    public void testAreFeesAmountChangedChangedFees() throws BuilderException {
        // Given
        BookingTracking alreadyTrackedBooking = getAlreadyTrackedBooking(CHANGED_AVOID_AMOUNT, PERKS_AMOUNT);
        setUpBookingMembershipFees();

        // When
        boolean feesAmountChanged = bookingTrackingOldModel.areFeesAmountChanged(alreadyTrackedBooking);

        // Then
        assertTrue(feesAmountChanged);
    }

    private BookingTracking getAlreadyTrackedBooking(BigDecimal avoidAmount, BigDecimal perksAmount) {
        BookingTracking alreadyTrackedBooking = new BookingTracking();
        alreadyTrackedBooking.setAvoidFeeAmount(avoidAmount);
        alreadyTrackedBooking.setPerksAmount(perksAmount);
        return alreadyTrackedBooking;
    }

    private void setUpBookingMembershipFees() throws BuilderException {
        bookingDetail.setAllFeeContainers(Arrays.asList(
            buildMembershipFeeContainer(buildFee(AVOID_FEES, AVOID_FEE_AMOUNT)),
            buildMembershipFeeContainer(buildFee(PERKS, PERKS_AMOUNT))));
    }

    private Fee buildFee(MembershipVatModelFee fee, BigDecimal amount) throws BuilderException {
        return new FeeBuilder().amount(amount).subCode(fee.getFeeCode()).build(RANDOM);
    }

    private FeeContainer buildMembershipFeeContainer(Fee... fees) {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(FEE_CONTAINER_ID);
        feeContainer.setFees(Arrays.asList(fees.clone()));
        return feeContainer;
    }

    private BookingDetail buildBookingDetail() throws BuilderException {
        Calendar bookingCalendar = Calendar.getInstance();
        bookingCalendar.setTime(new Date());
        return new BookingDetailBuilder()
            .bookingBasicInfoBuilder(new BookingBasicInfoBuilder()
                .id(BOOKING_ID).creationDate(bookingCalendar)
                .website(new WebsiteBuilder().brand(BRAND_ED).code(SITE_ES)))
            .bookingStatus(CONTRACT)
            .membershipPerks(new MembershipPerksBuilder().memberId(MEMBERSHIP_ID).feeContainerId(FEE_CONTAINER_ID))
            .build(RANDOM);
    }
}