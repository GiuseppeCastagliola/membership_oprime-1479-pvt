package com.odigeo.membership.robots.activator;

import bean.test.BeanTest;

public class BookingUpdatesConsumerConfigurationTest extends BeanTest<BookingUpdatesConsumerConfiguration> {

    @Override
    protected BookingUpdatesConsumerConfiguration getBean() {
        return new BookingUpdatesConsumerConfiguration();
    }
}
