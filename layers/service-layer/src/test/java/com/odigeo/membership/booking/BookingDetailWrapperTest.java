package com.odigeo.membership.booking;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.bookingapi.v12.responses.MembershipPerks;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.member.MemberService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.COST_FEE;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class BookingDetailWrapperTest {
    private static final String BOOKING_STATUS_CONTRACT = "CONTRACT";
    private static final String WEBSITE_BRAND = "ED";
    private static final String WEBSITE_CODE_IT = "IT";
    private static final BigDecimal MINUS_TEN = new BigDecimal("-10");
    private static final BigDecimal MINUS_TWENTY = new BigDecimal("-20");
    private static final long BOOKING_ID = 123L;
    private static final long FEE_CONTAINER_ID = 456L;
    private static final long MEMBERSHIP_ID = 789L;
    private static final String CURRENCY = "EUR";
    private static final Random RANDOM = new Random();
    private BookingDetailWrapper bookingDetailWrapper;

    @Mock
    private BookingDetail bookingDetail;
    @Mock
    private MemberService memberService;

    @BeforeMethod
    public void setUp() throws BuilderException {
        initMocks(this);
        configureBookingDetail(FEE_CONTAINER_ID, false);
        ConfigurationEngine.init(this::configure);
        bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
    }

    @Test
    public void testNullConstructor() {
        bookingDetailWrapper = new BookingDetailWrapper(null);
        assertNotNull(bookingDetailWrapper);
    }

    @Test
    public void testGetBookingId() {
        assertEquals(BOOKING_ID, bookingDetailWrapper.getBookingId());
    }

    @Test
    public void testGetMembershipPerksFeeContainerOldModel() {
        Optional<FeeContainer> membershipPerksFeeContainer = bookingDetailWrapper.getMembershipPerksFeeContainer();
        assertTrue(membershipPerksFeeContainer.isPresent());
        assertEquals(membershipPerksFeeContainer.get().getFees().size(), 3);
    }

    @Test
    public void testGetMembershipPerksFeeContainerNewVatModel() throws BuilderException {
        configureBookingDetail(FEE_CONTAINER_ID, true);
        Optional<FeeContainer> membershipPerksFeeContainer = bookingDetailWrapper.getMembershipPerksFeeContainer();
        assertTrue(membershipPerksFeeContainer.isPresent());
        assertEquals(membershipPerksFeeContainer.get().getFees().size(), 4);
    }

    @Test
    public void testGetMembershipPerksFeeContainerWrongId() throws BuilderException {
        configureBookingDetail(0L, false);
        assertFalse(bookingDetailWrapper.getMembershipPerksFeeContainer().isPresent());
    }

    @Test
    public void testGetMembershipPerksFeeContainerAbsent() {
        when(bookingDetail.getAllFeeContainers()).thenReturn(Collections.EMPTY_LIST);
        assertFalse(bookingDetailWrapper.getMembershipPerksFeeContainer().isPresent());
    }

    @Test
    public void testGetMembershipPerksNull() {
        when(bookingDetail.getMembershipPerks()).thenReturn(null);
        assertFalse(bookingDetailWrapper.getMembershipPerksFeeContainer().isPresent());
    }

    @Test
    public void testCalculateFeeAmountOldModel() {
        assertEquals(bookingDetailWrapper.calculateFeeAmount(AVOID_FEES), MINUS_TWENTY);
        assertEquals(bookingDetailWrapper.calculateFeeAmount(PERKS), MINUS_TEN);
    }

    @Test
    public void testCalculateFeeAmountNewVatModel() throws BuilderException {
        configureBookingDetail(FEE_CONTAINER_ID, true);
        assertEquals(bookingDetailWrapper.calculateFeeAmount(AVOID_FEES), MINUS_TWENTY);
        assertEquals(bookingDetailWrapper.calculateFeeAmount(COST_FEE), MINUS_TEN);
        assertEquals(bookingDetailWrapper.calculateFeeAmount(PERKS), MINUS_TEN);
    }

    @Test
    public void testCalculateFeeAmountWithNoPerks() {
        when(bookingDetail.getAllFeeContainers()).thenReturn(null);
        assertEquals(bookingDetailWrapper.calculateFeeAmount(AVOID_FEES), BigDecimal.ZERO);
    }

    @Test
    public void testCreateBookingTrackingOldModel() {
        BookingTracking bookingTracking = bookingDetailWrapper.createBookingTracking(MEMBERSHIP_ID);
        assertEquals(bookingTracking.getBookingId(), BOOKING_ID);
        assertEquals(bookingTracking.getAvoidFeeAmount(), MINUS_TWENTY);
        assertEquals(bookingTracking.getCostFeeAmount(), BigDecimal.ZERO);
        assertEquals(bookingTracking.getPerksAmount(), MINUS_TEN);
    }

    @Test
    public void testCreateBookingTrackingNewVatModel() throws BuilderException {
        configureBookingDetail(FEE_CONTAINER_ID, true);
        BookingTracking bookingTracking = bookingDetailWrapper.createBookingTracking(MEMBERSHIP_ID);
        assertEquals(bookingTracking.getBookingId(), BOOKING_ID);
        assertEquals(bookingTracking.getAvoidFeeAmount(), MINUS_TWENTY);
        assertEquals(bookingTracking.getCostFeeAmount(), MINUS_TEN);
        assertEquals(bookingTracking.getPerksAmount(), MINUS_TEN);
    }

    @Test
    public void testGetCurrencyCode() {
        assertEquals(bookingDetailWrapper.getCurrencyCode(), CURRENCY);
    }

    @Test
    public void testIsContract() {
        assertTrue(bookingDetailWrapper.isContract());
    }

    @Test
    public void testIsPrime() {
        assertTrue(bookingDetailWrapper.isPrimeBooking());
    }

    @Test
    public void testGetBookingDetail() {
        assertEquals(bookingDetailWrapper.getBookingDetail(), bookingDetail);
    }

    @Test
    public void testGetMemberId() {
        assertEquals(bookingDetailWrapper.getMemberId(), MEMBERSHIP_ID);
    }

    private void configureBookingDetail(long feeContainerId, boolean isNewVatModel) throws BuilderException {
        when(bookingDetail.getBookingBasicInfo()).thenReturn(createBookingBasicInfo());
        when(bookingDetail.getMembershipPerks()).thenReturn(createMembershipPerks());
        if (isNewVatModel) {
            when(bookingDetail.getAllFeeContainers()).thenReturn(Collections.singletonList(createFeeContainer(feeContainerId, true)));
        } else {
            when(bookingDetail.getAllFeeContainers()).thenReturn(Collections.singletonList(createFeeContainer(feeContainerId, false)));
        }
        when(bookingDetail.getBookingStatus()).thenReturn(BOOKING_STATUS_CONTRACT);
        when(bookingDetail.getCurrencyCode()).thenReturn(CURRENCY);
    }

    private BookingBasicInfo createBookingBasicInfo() throws BuilderException {
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfoBuilder()
                .id(BOOKING_ID)
                .website(new WebsiteBuilder().code(WEBSITE_CODE_IT).brand(WEBSITE_BRAND))
                .build(RANDOM);
        bookingBasicInfo.setMembershipId(MEMBERSHIP_ID);
        return bookingBasicInfo;
    }

    private FeeContainer createFeeContainer(long feeContainerId, boolean isNewVatModel) throws BuilderException {
        return new FeeContainerBuilder()
                .id(feeContainerId)
                .fees(getFeeList(isNewVatModel))
                .build(RANDOM);
    }

    private List<FeeBuilder> getFeeList(boolean isNewVatModel) {
        List<FeeBuilder> feeList = new ArrayList<>();
        feeList.add(new FeeBuilder().subCode(AVOID_FEES.getFeeCode()).feeLabel(AVOID_FEES.name()).amount(MINUS_TEN).currency(CURRENCY));
        feeList.add(new FeeBuilder().subCode(AVOID_FEES.getFeeCode()).feeLabel(AVOID_FEES.name()).amount(MINUS_TEN).currency(CURRENCY));
        feeList.add(new FeeBuilder().subCode(PERKS.getFeeCode()).feeLabel(PERKS.name()).amount(MINUS_TEN).currency(CURRENCY));
        if (isNewVatModel) {
            feeList.add(new FeeBuilder().subCode(COST_FEE.getFeeCode()).feeLabel(AVOID_FEES.name()).amount(MINUS_TEN).currency(CURRENCY));
        }
        return feeList;
    }

    private MembershipPerks createMembershipPerks() throws BuilderException {
        return new MembershipPerksBuilder().feeContainerId(FEE_CONTAINER_ID)
                .memberId(MEMBERSHIP_ID)
                .build(RANDOM);
    }

    private void configure(Binder binder) {
        binder.bind(MemberService.class).toProvider(() -> memberService);
    }
}