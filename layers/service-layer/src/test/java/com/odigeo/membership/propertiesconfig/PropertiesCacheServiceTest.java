package com.odigeo.membership.propertiesconfig;


import com.edreams.persistance.cache.Cache;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.Serializable;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PropertiesCacheServiceTest {

    private static final Serializable SEND_IDS_TO_KAFKA_CACHE_KEY = "sendIdsKafkaActive";
    private static final Serializable SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY = "sendTransactionalWelcomeEmailActive";

    private PropertiesConfigurationService propertiesConfigurationServiceMock;
    private Cache cacheMock;
    private PropertiesCacheService propertiesCacheService;

    @BeforeMethod
    public void setUp() {
        final PropertiesCacheConfiguration propertiesCacheConfigurationMock = mock(PropertiesCacheConfiguration.class);
        propertiesConfigurationServiceMock = mock(PropertiesConfigurationService.class);
        propertiesCacheService = new PropertiesCacheService(propertiesCacheConfigurationMock, propertiesConfigurationServiceMock);

        cacheMock = mock(Cache.class);
        given(propertiesCacheConfigurationMock.getIsSendingIdsToKafkaActiveCache()).willReturn(cacheMock);
        given(propertiesCacheConfigurationMock.getIsTransactionalWelcomeEmailActiveCache()).willReturn(cacheMock);
    }

    @Test
    public void testSendIdsToKafkaTrueWhenTrueInCache() {
        //Given
        given(cacheMock.fetchValue(SEND_IDS_TO_KAFKA_CACHE_KEY)).willReturn(TRUE);
        //When
        boolean sendingIdsToKafkaActive = propertiesCacheService.isSendingIdsToKafkaActive();
        //Then
        assertTrue(sendingIdsToKafkaActive);
    }

    @Test
    public void testSendIdsToKafkaTrueWhenNotInCacheAndTrue() {
        //Given
        given(cacheMock.fetchValue(SEND_IDS_TO_KAFKA_CACHE_KEY)).willReturn(null);
        given(propertiesConfigurationServiceMock.isSendingIdsToKafkaActive()).willReturn(true);
        //When
        boolean sendingIdsToKafkaActive = propertiesCacheService.isSendingIdsToKafkaActive();
        //Then
        assertTrue(sendingIdsToKafkaActive);
    }

    @Test
    public void testSendIdsToKafkaFalseWhenFalseInCache() {
        //Given
        given(cacheMock.fetchValue(SEND_IDS_TO_KAFKA_CACHE_KEY)).willReturn(FALSE);
        //When
        boolean sendingIdsToKafkaActive = propertiesCacheService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testSendIdsToKafkaFalseWhenNotInCacheAndFalse() {
        //Given
        given(cacheMock.fetchValue(SEND_IDS_TO_KAFKA_CACHE_KEY)).willReturn(null);
        given(propertiesConfigurationServiceMock.isSendingIdsToKafkaActive()).willReturn(false);
        //When
        boolean sendingIdsToKafkaActive = propertiesCacheService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testRefreshIsSendingIdsToKafkaToTrue() {
        //Given
        given(propertiesConfigurationServiceMock.isSendingIdsToKafkaActive()).willReturn(TRUE);
        //When
        propertiesCacheService.refreshIsSendingIdsToKafkaActive();
        //Then
        verify(cacheMock).addEntry(SEND_IDS_TO_KAFKA_CACHE_KEY, TRUE);
    }

    @Test
    public void testRefreshIsSendingIdsToKafkaToFalse() {
        //Given
        given(propertiesConfigurationServiceMock.isSendingIdsToKafkaActive()).willReturn(FALSE);
        //When
        propertiesCacheService.refreshIsSendingIdsToKafkaActive();
        //Then
        verify(cacheMock).addEntry(SEND_IDS_TO_KAFKA_CACHE_KEY, FALSE);
    }

    @Test
    public void testTransactionalWelcomeEmailTrueWhenTrueInCache() {
        //Given
        given(cacheMock.fetchValue(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY)).willReturn(TRUE);
        //When
        boolean transactionalWelcomeEmailActive = propertiesCacheService.isTransactionalWelcomeEmailActive();
        //Then
        assertTrue(transactionalWelcomeEmailActive);
    }

    @Test
    public void testTransactionalWelcomeEmailTrueWhenNotInCacheAndTrue() {
        //Given
        given(cacheMock.fetchValue(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY)).willReturn(null);
        given(propertiesConfigurationServiceMock.isTransactionalWelcomeEmailActive()).willReturn(true);
        //When
        boolean transactionalWelcomeEmailActive = propertiesCacheService.isTransactionalWelcomeEmailActive();
        //Then
        assertTrue(transactionalWelcomeEmailActive);
    }

    @Test
    public void testTransactionalWelcomeEmailFalseWhenFalseInCache() {
        //Given
        given(cacheMock.fetchValue(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY)).willReturn(FALSE);
        //When
        boolean transactionalWelcomeEmailActive = propertiesCacheService.isTransactionalWelcomeEmailActive();
        //Then
        assertFalse(transactionalWelcomeEmailActive);
    }

    @Test
    public void testTransactionalWelcomeEmailFalseWhenNotInCacheAndFalse() {
        //Given
        given(cacheMock.fetchValue(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY)).willReturn(null);
        given(propertiesConfigurationServiceMock.isTransactionalWelcomeEmailActive()).willReturn(false);
        //When
        boolean transactionalWelcomeEmailActive = propertiesCacheService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(transactionalWelcomeEmailActive);
    }

    @Test
    public void testRefreshIsTransactionalWelcomeEmailToTrue() {
        //Given
        given(propertiesConfigurationServiceMock.isTransactionalWelcomeEmailActive()).willReturn(TRUE);
        //When
        propertiesCacheService.refreshIsTransactionalWelcomeEmailActive();
        //Then
        verify(cacheMock).addEntry(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY, TRUE);
    }

    @Test
    public void testRefreshIsTransactionalWelcomeEmailToFalse() {
        //Given
        given(propertiesConfigurationServiceMock.isTransactionalWelcomeEmailActive()).willReturn(FALSE);
        //When
        propertiesCacheService.refreshIsTransactionalWelcomeEmailActive();
        //Then
        verify(cacheMock).addEntry(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY, FALSE);
    }
}