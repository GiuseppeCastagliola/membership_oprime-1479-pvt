package com.odigeo.messaging;

import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertEquals;

public class SubscriptionMessagePublisherTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long USER_ID = 343L;
    private static final String EMAIL = "user@odigeo.com";
    private static final Long BOOKING_ID = 200L;
    private static final Membership ACTIVE_MEMBERSHIP = new MembershipBuilder()
            .setId(MEMBERSHIP_ID)
            .setStatus(MemberStatus.ACTIVATED)
            .setMemberAccount(new MemberAccount(404L, USER_ID, "Test", "User")).build();

    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingServiceMock;
    @Mock
    private MembershipSubscriptionMessageService membershipSubscriptionMessageServiceMock;

    private SubscriptionMessagePublisher subscriptionMessagePublisher;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        subscriptionMessagePublisher = new SubscriptionMessagePublisher(membershipMessageSendingServiceMock, membershipSubscriptionMessageServiceMock);
    }

    @AfterMethod
    public void tearDown() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicHappyPathWithSuccessfulCRMMsg() throws DataNotFoundException {
        MembershipSubscriptionMessage subscriptionMessage = mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.getMembershipSubscriptionMessage(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED))
                .thenReturn(subscriptionMessage);
        when(membershipMessageSendingServiceMock.sendCompletedMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class)))
                .thenReturn(true);
        assertTrue(subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED));
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(subscriptionMessage);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicHappyPathWithFailedCRMMsg() throws DataNotFoundException {
        MembershipSubscriptionMessage subscriptionMessage = mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.getMembershipSubscriptionMessage(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED))
                .thenReturn(subscriptionMessage);
        when(membershipMessageSendingServiceMock.sendCompletedMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class)))
                .thenReturn(false);
        assertFalse(subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED));
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(subscriptionMessage);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopic_ExceptionRetrievingUserInfo() throws DataNotFoundException {
        doThrow(new DataNotFoundException(StringUtils.EMPTY))
                .when(membershipSubscriptionMessageServiceMock).getMembershipSubscriptionMessage(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED);
        assertFalse(subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED));
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_HappyPath() throws DataNotFoundException {
        final UserInfo userInfo = getUserInfo();
        MembershipSubscriptionMessage subscriptionMessage = mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.buildMembershipSubscriptionMessage(userInfo, ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED))
                .thenReturn(subscriptionMessage);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(userInfo, ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(subscriptionMessage);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_ExceptionRetrievingUserInfo() throws DataNotFoundException {
        final UserInfo userInfo = getUserInfo();
        doThrow(new DataNotFoundException(StringUtils.EMPTY))
                .when(membershipSubscriptionMessageServiceMock).buildMembershipSubscriptionMessage(userInfo, ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(userInfo, ACTIVE_MEMBERSHIP, SubscriptionStatus.SUBSCRIBED);
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendWelcomeToPrimeMessageToMembershipTransactionalTopic_HappyPath() {
        ArgumentCaptor<MembershipMailerMessage> argument = ArgumentCaptor.forClass(MembershipMailerMessage.class);
        subscriptionMessagePublisher.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(ACTIVE_MEMBERSHIP, BOOKING_ID, false);

        verify(membershipMessageSendingServiceMock).sendMembershipMailerMessage(argument.capture(), eq(false));
        assertEquals(argument.getValue().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(argument.getValue().getBookingId(), BOOKING_ID);
        assertEquals(argument.getValue().getUserId(), USER_ID);
        assertEquals(argument.getValue().getMessageType(), WELCOME_TO_PRIME);
    }

    private void verifyLogEventsWhenExceptionOccurred() {
        verify(mockAppender, times(2)).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        assertEquals(loggingEvents.size(), 2);
        assertEquals(Level.INFO, loggingEvents.get(0).getLevel());
        assertEquals(Level.WARN, loggingEvents.get(1).getLevel());
    }

    private UserInfo getUserInfo() {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setId(USER_ID);
        return UserInfo.fromUserBasicInfo(userBasicInfo);
    }
}
