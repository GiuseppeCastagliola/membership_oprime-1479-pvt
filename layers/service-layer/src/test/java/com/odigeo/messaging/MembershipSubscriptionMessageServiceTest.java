package com.odigeo.messaging;

import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.Status;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipSubscriptionMessageServiceTest {
    private static final String EMAIL = "email";
    private static final String WEBSITE = "website";
    private static final Long USER_ID = 1L;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = ACTIVATION_DATE.plusMonths(6);
    private static final int MONTHS_DURATION = 6;

    @Mock
    private UserApiManager userApiManagerMock;

    private MembershipSubscriptionMessageService membershipSubscriptionMessageService;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipSubscriptionMessageService = new MembershipSubscriptionMessageService(userApiManagerMock);
    }

    @Test
    public void testCompleteSubscriptionMessage_NoUserBasicInfo() {
        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.defaultUserInfo());

        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();
        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.completeSubscriptionMessage(defaultMembershipSubscriptionMessage);

        assertEqualsMembershipSubscriptionFields(membershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        assertNull(membershipSubscriptionMessage.getForgetPasswordToken());
        assertFalse(membershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test
    public void testCompleteSubscriptionMessage_WithNotPendingLoginUserBasicInfo() {
        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();
        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.completeSubscriptionMessage(defaultMembershipSubscriptionMessage);

        assertEqualsMembershipSubscriptionFields(membershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        assertNull(membershipSubscriptionMessage.getForgetPasswordToken());
        assertFalse(membershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test
    public void testCompleteSubscriptionMessage_WithPendingLoginUserBasicInfoAndNoToken() {
        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(true)));
        when(userApiManagerMock.getForgetPasswordToken(USER_ID)).thenReturn(Optional.empty());

        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();
        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.completeSubscriptionMessage(defaultMembershipSubscriptionMessage);

        assertEqualsMembershipSubscriptionFields(membershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        assertNull(membershipSubscriptionMessage.getForgetPasswordToken());
        assertFalse(membershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test
    public void testCompleteSubscriptionMessage_WithPendingLoginUserBasicInfoAndToken() {
        final String token = "token";

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(true)));
        when(userApiManagerMock.getForgetPasswordToken(USER_ID)).thenReturn(Optional.of(token));

        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();
        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.completeSubscriptionMessage(defaultMembershipSubscriptionMessage);

        assertEqualsMembershipSubscriptionFields(membershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        assertEquals(membershipSubscriptionMessage.getForgetPasswordToken(), token);
        assertTrue(membershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMembershipSubscriptionMessage_NullMemberAccount() throws DataNotFoundException {
        final Membership membership = new MembershipBuilder().build();

        membershipSubscriptionMessageService.getMembershipSubscriptionMessage(membership, SubscriptionStatus.SUBSCRIBED);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMembershipSubscriptionMessage_NullEmail() throws DataNotFoundException {
        final Membership membership = getMembership();
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.defaultUserInfo());

        membershipSubscriptionMessageService.getMembershipSubscriptionMessage(membership, SubscriptionStatus.SUBSCRIBED);
    }

    @Test
    public void testGetMembershipSubscriptionMessage_HappyPath() throws DataNotFoundException {
        final Membership membership = getMembership();
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.getMembershipSubscriptionMessage(membership, SubscriptionStatus.SUBSCRIBED);

        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessage(), membershipSubscriptionMessage);
    }

    @Test
    public void testGetMembershipSubscriptionMessage_MemberWithoutActivationDate() throws DataNotFoundException {
        final Membership membership = getPendingToCollectMembership();
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService.getMembershipSubscriptionMessage(membership, SubscriptionStatus.SUBSCRIBED);

        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessageForPTC(), membershipSubscriptionMessage);
    }

    private Membership getMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        return new MembershipBuilder().setWebsite(WEBSITE)
                .setActivationDate(ACTIVATION_DATE).setExpirationDate(EXPIRATION_DATE)
                .setMemberAccount(memberAccount).build();
    }

    private Membership getPendingToCollectMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        return new MembershipBuilder().setWebsite(WEBSITE)
                .setStatus(MemberStatus.PENDING_TO_COLLECT)
                .setMonthsDuration(MONTHS_DURATION)
                .setExpirationDate(EXPIRATION_DATE)
                .setMemberAccount(memberAccount).build();
    }

    private void assertEqualsMembershipSubscriptionFields(MembershipSubscriptionMessage expected, MembershipSubscriptionMessage actual) {
        assertEquals(actual.getEmail(), expected.getEmail());
        assertEquals(actual.getSubscriptionStatus(), expected.getSubscriptionStatus());
        assertEquals(actual.getActivationDate(), expected.getActivationDate());
        assertEquals(actual.getExpirationDate(), expected.getExpirationDate());
        assertEquals(actual.getWebsite(), expected.getWebsite());
        assertEquals(actual.getKey(), expected.getKey());
        assertEquals(actual.getTopicName(), expected.getTopicName());
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessage() {
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED)
                .withDates(ACTIVATION_DATE.toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .build();
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessageForPTC() {
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED)
                .withDates(EXPIRATION_DATE.minusMonths(MONTHS_DURATION).toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .build();
    }

    private UserBasicInfo sampleUserBasicInfo(boolean isPendingLogin) {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setStatus(isPendingLogin ? Status.PENDING_LOGIN : Status.ACTIVE);
        return userBasicInfo;
    }
}