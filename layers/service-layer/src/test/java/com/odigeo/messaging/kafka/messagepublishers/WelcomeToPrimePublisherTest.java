package com.odigeo.messaging.kafka.messagepublishers;

import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.apache.kafka.clients.producer.Callback;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class WelcomeToPrimePublisherTest {
    @Mock
    private KafkaPublisher<Message> kafkaPublisher;

    @InjectMocks
    private WelcomeToPrimePublisher welcomeToPrimePublisher = new WelcomeToPrimePublisher();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void publishMessageTest() {
        welcomeToPrimePublisher.publishMembershipMailerMessage(aMembershipMailerMessage(), getCallback());
        verify(kafkaPublisher).publishMessage(any(MembershipMailerMessage.class), any(Callback.class));
    }

    @Test
    public void handlesPublishMessageException() {
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException())).when(kafkaPublisher).publishMessage(any(), any());
        welcomeToPrimePublisher.publishMembershipMailerMessage(aMembershipMailerMessage(), getCallback());
    }

    private MembershipMailerMessage aMembershipMailerMessage() {
        return new MembershipMailerMessage.Builder()
                .withMembershipId(222L)
                .withUserId(333L)
                .withBookingId(444L)
                .build();
    }

    private Callback getCallback() {
        return (recordMetadata,e)-> {};
    }
}
