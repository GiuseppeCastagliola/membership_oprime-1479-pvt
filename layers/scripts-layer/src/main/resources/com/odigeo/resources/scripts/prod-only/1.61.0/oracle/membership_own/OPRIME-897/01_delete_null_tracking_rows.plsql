--SET DEFINE OFF;
--SET SERVEROUTPUT ON;

DECLARE
    affected_rows NUMBER := 0;
    CURSOR regs IS
    SELECT booking_id FROM membership_own.ge_membership_booking_tracking WHERE avoid_fee_amount is NULL and rownum <= 10;

BEGIN
    --dbms_output.put_line('--Start of the PLSQL block');
    FOR reg IN regs LOOP
        affected_rows := affected_rows + 1;
        DELETE FROM membership_own.ge_membership_booking_tracking WHERE booking_id = reg.booking_id;

        IF affected_rows = 5000 THEN
            COMMIT;
            affected_rows := 0;
        END IF;
    END LOOP;
    COMMIT;
    --dbms_output.put_line('--End of the PLSQL block');
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END;
