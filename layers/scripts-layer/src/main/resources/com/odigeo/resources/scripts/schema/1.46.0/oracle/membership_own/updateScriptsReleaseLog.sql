UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.46.0' where ID ='OPRIME-709/01_create_index_ge_membership.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.46.0' where ID ='OPRIME-709/02_create_index_ge_member_account.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.46.0' where ID ='rollback/R_01_create_index_ge_membership.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.46.0' where ID ='rollback/R_02_create_index_ge_member_account.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.46.0',systimestamp,'membership');
