package com.odigeo.membership.robots.tracker;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.robots.RobotExecutionResult;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class MembershipTrackerRobotTest {

    private static final String TEST_ROBOT = "TestRobot";
    private MembershipTrackerRobot membershipTrackerRobot;
    @Mock
    private BookingUpdatesReaderService bookingUpdatesReaderService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipTrackerRobot = new MembershipTrackerRobot(TEST_ROBOT);
    }

    private void configure(Binder binder) {
        binder.bind(BookingUpdatesReaderService.class).toInstance(bookingUpdatesReaderService);
    }

    @Test
    public void testExecuteCritical() {
        assertEquals(membershipTrackerRobot.execute(Collections.emptyMap()), RobotExecutionResult.CRITICAL);
    }

    @Test
    public void testExecuteOK() {
        when(bookingUpdatesReaderService.startTrackerConsumerController()).thenReturn(true);
        assertEquals(membershipTrackerRobot.execute(Collections.emptyMap()), RobotExecutionResult.OK);
    }

    @Test
    public void testInterrupt() {
        membershipTrackerRobot.execute(Collections.emptyMap());
        membershipTrackerRobot.interrupt();
        verify(bookingUpdatesReaderService).stopTrackerConsumerController();
    }
}
